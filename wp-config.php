<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cms_assignment' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'uz@+j@~qS8.okMq2>yH>AkNS}Pg5L<FSTNVOau?04T?>A-v7$F92w<mx%`vT@l;`' );
define( 'SECURE_AUTH_KEY',  ',CdcQ<Tt, HmGjNm@iR2~s?K3#y6@>JC^[3(CZGq.C&<Yna6^(1Gn^(2<^xo[<c9' );
define( 'LOGGED_IN_KEY',    'Fpf>AVN?dz5mh.e:e,C.?,~.,7&Ou79ZZQA?2MD(fFCIEs87qK8k@}KCm!EN9r+8' );
define( 'NONCE_KEY',        'Z6$,=?[{xW Lk&pP0kR[*?S0G[ gPMoFtvF,QtH|w*fkSs:e[]C;^Ru,AUy)lH?}' );
define( 'AUTH_SALT',        'WAqvM.VGs.*V%+= I) &QpjbDf+/SB?b|MW_6J%L#{F~OexS-dSxIv](!`5,)}2(' );
define( 'SECURE_AUTH_SALT', '1UhHG.),~/WPv0^y<cE;wj{;<|7*pPu1}c4&q%K34/Hd9-[yy274:z}_Vb1,ZC*8' );
define( 'LOGGED_IN_SALT',   'JCoAT0)X;L-<wJx-#E1/=x^TUSj00S[{Bsp^<C}1 }}:J#XV,CMOfEOnFW9(H/%;' );
define( 'NONCE_SALT',       'z}9|np3rwHh2x=/F8OXllRYyEe|Zg~Jlnc7ism;<-C$X7-/F;DthdD;RQ6Y|g{R~' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
